//
//  ViewController.swift
//  Project27
//
//  Created by Muri Gumbodete on 19/04/2022.
//

import UIKit

class ViewController: UIViewController {
  //MARK: - IBOutlets
  @IBOutlet var imageView: UIImageView!
  
  // MARK: - Variables
  var currentDrawType = 0
  
  // MARK: - Lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()
    drawRectangle()
  }

  // MARK: - IBAction Functions
  @IBAction func redrawTapped(_ sender: Any) {
    currentDrawType += 5
    
    if currentDrawType > 5 {
      currentDrawType = 0
    }
    
    switch currentDrawType {
    case 0:
      drawRectangle()
    default:
      break
    }
  }
}

// MARK: - Private Methods
extension ViewController {
  func drawRectangle() {
    let renderer = UIGraphicsImageRenderer(size: CGSize(width: 512, height: 512))
    
    let image = renderer.image { ctx in
      let rectangle = CGRect(x: 0, y: 0, width: 512, height: 512)
      
      ctx.cgContext.setFillColor(UIColor.red.cgColor)
      ctx.cgContext.setStrokeColor(UIColor.black.cgColor)
      ctx.cgContext.setLineWidth(10)
      
      ctx.cgContext.addRect(rectangle)
      ctx.cgContext.drawPath(using: .fillStroke)
      
    }
    
    imageView.image = image
  }
}

